require("@digiresilience/eslint-config-amigo/patch/modern-module-resolution");
module.exports = {
  extends: [
    "@digiresilience/eslint-config-amigo/profile/node",
    "@digiresilience/eslint-config-amigo/profile/typescript"
  ],
  parserOptions: { tsconfigRootDir: __dirname, project: "./tsconfig.spec.json"  },
  rules: {
    "no-prototype-builtins": "off",
    "@typescript-eslint/no-empty-function": "off",
    "no-unused-vars": [
      "error",
      {
        argsIgnorePattern: "^_",
      },
    ],
    "@typescript-eslint/no-unused-vars": [
      "error",
      {
        argsIgnorePattern: "^_",
      },
    ],
  },
};
