import { mutableProxyFactory, PProxyHandler } from "./proxy";

describe("mutable proxy types", () => {
  test("function", async () => {
    const { proxy, setTarget } = mutableProxyFactory(() => 42);
    expect(proxy()).toBe(42);
    setTarget(() => 43);
    expect(proxy()).toBe(43);
  });

  test("object", async () => {
    const { proxy, setTarget } = mutableProxyFactory({ value: 42 });
    expect(proxy.value).toBe(42);
    setTarget({ value: 43 });
    expect(proxy.value).toBe(43);
  });

  test("array", async () => {
    const { proxy, setTarget } = mutableProxyFactory([42]);
    expect(proxy[0]).toBe(42);
    setTarget([43]);
    expect(proxy[0]).toBe(43);
  });

  test("object to function", async () => {
    const { proxy, setTarget } = mutableProxyFactory({ value: 42 });
    expect(proxy.value).toBe(42);
    setTarget(() => 43);
    expect(() => proxy()).toThrow();
  });
  test("scalar", async () => {
    expect(() => {
      mutableProxyFactory(42);
    }).toThrow();
  });

  test("boolean", async () => {
    expect(() => {
      mutableProxyFactory(false);
    }).toThrow();
  });

  test("null", async () => {
    expect(() => {
      mutableProxyFactory(null); // eslint-disable-line unicorn/no-null
    }).toThrow();
  });

  test("undefined", async () => {
    expect(() => {
      mutableProxyFactory();
    }).toThrow();
  });

  test("setHandler", async () => {
    const { proxy, setHandler } = mutableProxyFactory({ value: 42 });
    setHandler(new PProxyHandler());
    expect(proxy.value).toBe(42);
  });

  test("setTarget", async () => {
    const { proxy, setTarget } = mutableProxyFactory({ value: 42 });
    setTarget([43]);
    expect(proxy[0]).toBe(43);
  });

  test("setHandler simple", async () => {
    const { proxy, setHandler } = mutableProxyFactory({ value: 41 });
    expect(proxy.value).toBe(41);
    setHandler({
      get: () => 42,
    });
    expect(proxy.value).toBe(42);
  });

  test("getHandler", async () => {
    const { getHandler, setHandler, proxy } = mutableProxyFactory({
      value: 42,
    });
    const handler = new PProxyHandler();
    setHandler(handler);
    expect(getHandler()).toBe(handler);
    expect(proxy.value).toBe(42);
  });

  test("getTarget", async () => {
    const { getTarget } = mutableProxyFactory({ value: 42 });
    expect(getTarget().value).toBe(42);
  });
});
