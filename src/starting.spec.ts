import { defState, stop, startOnly, startWithout } from ".";

const startOnlyState = defState("startOnlyState", {
  start: () => ({
    value: 42,
  }),
});

const startedState = defState("startState", {
  start: () => ({
    value: 42,
  }),
});

const neverStartedState = defState("neverStartedState", {
  start: () => ({
    value: 42,
  }),
});

describe("starting", () => {
  afterEach(async () => stop());

  test("startOnly", async () => {
    await startOnly(["startOnlyState"]);
    expect(startOnlyState.value).toBe(42);
    expect(startedState.value).toBe(undefined);
    expect(neverStartedState.value).toBe(undefined);
  });

  test("startWithout", async () => {
    await startWithout(["neverStartedState"]);
    expect(startOnlyState.value).toBe(42);
    expect(startedState.value).toBe(42);
    expect(neverStartedState.value).toBe(undefined);
  });
});
