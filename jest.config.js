module.exports = {
  testEnvironment: 'node',
  roots: ["<rootDir>", "<rootDir>/src"],
  testMatch: ["<rootDir>/src/**/*.spec.{ts,tsx}"],
  silent: false,
  verbose: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
  testPathIgnorePatterns: [
    "/node_modules"
  ],

  /*
   The modulePathIgnorePatterns accepts these sorts of paths:
     <rootDir>/src
     <rootDir>/src/file.ts
    ...and ignores anything else under <rootDir>
    */
  modulePathIgnorePatterns: ["^<rootDir>/(?!(?:src/)|(?:src$))"]
};
